package com.hleofxquotes.examples.mny;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.TimeZone;

import org.apache.log4j.Logger;

import com.healthmarketscience.jackcess.Column;
import com.healthmarketscience.jackcess.CryptCodecProvider;
import com.healthmarketscience.jackcess.Database;
import com.healthmarketscience.jackcess.DatabaseBuilder;
import com.healthmarketscience.jackcess.Row;
import com.healthmarketscience.jackcess.Table;
import com.healthmarketscience.jackcess.impl.CodecProvider;
import com.healthmarketscience.jackcess.impl.DefaultCodecProvider;

/**
 * An example on how to open an MNY file.
 * 
 * @author hleofxquotes
 *
 */
public class OpenMnyFile {
    private static final Logger LOGGER = Logger.getLogger(OpenMnyFile.class);

    public static void main(String[] args) {
        String dbFileName = null;
        String dbPassword = null;

        // parse arguments: fileName and password
        if (args.length == 1) {
            dbFileName = args[0];
        } else if (args.length == 2) {
            dbFileName = args[0];
            dbPassword = args[1];
        } else {
            Class<OpenMnyFile> clz = OpenMnyFile.class;
            System.out.println("Usage: java " + clz.getName() + " mnyFile [password]");
            System.exit(1);
        }

        OpenMnyFile openMnyFile = new OpenMnyFile();
        try {
            // open the mny file
            Database db = openMnyFile.open(dbFileName, dbPassword);

            // iterate the list of table
            for (String tableName : db.getTableNames()) {
                Table table = db.getTable(tableName);
                LOGGER.info("# table=" + table.getName());
                LOGGER.info("  columns=" + table.getColumnCount());

                // get table columns and print out name, type
                for (Column column : table.getColumns()) {
                    LOGGER.info("    column.name=" + column.getName() + ", column.type=" + column.getType());
                }

                // iterate the rows
                int rowCount = 0;
                for (Row row : table) {
                    rowCount++;
                }
                LOGGER.info("  rows=" + rowCount);
            }

        } catch (IOException e) {
            LOGGER.error(e, e);
        }

    }

    /**
     * Open mny file with filename and password
     * 
     * @param dbFileName
     * @param dbPassword
     * @return
     * @throws IOException
     */
    public Database open(String dbFileName, String dbPassword) throws IOException {

        File dbFile = new File(dbFileName);

        // create the CodecProvider to decode mny encryption
        CodecProvider cryptCodecProvider = null;
        if (dbPassword == null) {
            cryptCodecProvider = new DefaultCodecProvider();
        } else {
            cryptCodecProvider = new CryptCodecProvider(dbPassword);
        }

        return open(dbFile, cryptCodecProvider);
    }

    /**
     * Open a mny file with given decoder.
     * 
     * @param dbFile
     * @param cryptCodecProvider
     * @return
     * @throws IOException
     */
    private Database open(File dbFile, CodecProvider cryptCodecProvider) throws IOException {
        boolean readOnly = true;
        boolean autoSync = true;
        Charset charset = null;
        TimeZone timeZone = TimeZone.getDefault();
        Database db = new DatabaseBuilder(dbFile).setReadOnly(readOnly).setAutoSync(autoSync).setCharset(charset)
                .setTimeZone(timeZone).setCodecProvider(cryptCodecProvider).open();

        return db;
    }

}
